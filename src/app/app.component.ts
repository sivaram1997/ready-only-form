import { Component } from '@angular/core';
import {  FormBuilder, FormGroup, Validators} from '@angular/forms'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ready-only-form';
  myarray:any[]=[ {name:'siva',name2:'arifa'}];
  myForm:FormGroup;
  valuereadonly:boolean=true;
  constructor(private fb:FormBuilder){
   this.myForm = this.fb.group({
      name:['',Validators.required],
      name2:['',Validators.required],
      data2:[],
      data1:[]

    });
  }
 add(){
    this.myForm.value.name;
    this.myForm.value.name2;
    this.myarray.push(this.myForm.value)
  }
  delete(i){
    this.myarray.splice(i,1)
  }
  edit(){
    this.valuereadonly=false;
  }
}
